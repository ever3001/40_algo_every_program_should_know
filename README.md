[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# 40 Algorithms Every Programmer Should Know

This is my implementations of 40 Algorithms Every Programmer Should Know: Hone Your Problem-solving Skills by Learning Different Algorithms and Their Implementation. 

Algorithms have always played an important role in both the science and practice of computing. Beyond traditional computing, the ability to use algorithms to solve real-world problems is an important skill that any developer or programmer must have.

I'm using Jupyter notebook for testing the algorithms.

## Project status

[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

## Summary

### Data Structures

- [ ] List
- [ ] Tuples
- [ ] Dictionary
- [ ] Sets
- [ ] DataFrames
- [ ] Matrix
- [ ] Vector
- [ ] Stacks
- [ ] Queues
- [ ] Tree


### Sorting Algorithms

- [ ] Swapping variables
- [ ] Bubble Sort
- [ ] Insertion Sort
- [ ] Merge Sort
- [ ] Shell Sort
- [ ] Selection Sort

### Searching Algorithms

- [ ] Linear Search
- [ ] Binary Search
- [ ] Interpolation Search

### Designing Algorithms

TODO: Create list
### Graph Algorithms

TODO: Create list

### Unsupervised MachineLearning Algorithms

TODO: Create list

### Traditional Supervised Learning Algorithms

TODO: Create list

### Neural Network Algorithms

TODO: Create list

### Algorithms for Natural Language Processing

TODO: Create list
### Data Algorithms

TODO: Create list
### Cryptography

TODO: Create list

### Large-Scale Algorithms

TODO: Create list

## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE) file for details.
